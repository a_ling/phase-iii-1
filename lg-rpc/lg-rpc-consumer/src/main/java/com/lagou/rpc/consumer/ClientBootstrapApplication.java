package com.lagou.rpc.consumer;

import com.lagou.rpc.api.IUserService;
import com.lagou.rpc.consumer.proxy.RpcClientProxy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @Author : liuchangling
 * @Descrition :
 * @Date： Created in 11:05 上午 2021/12/3
 */
@SpringBootApplication
public class ClientBootstrapApplication {
        public static void main(String[] args) {
            SpringApplication.run(ClientBootstrapApplication.class, args);
        }

        //添加注解，注册到ioc容器中，便于在controller中调用
        @Bean
        public IUserService IUserService() {
            IUserService userService = (IUserService) RpcClientProxy.createProxy(IUserService.class);
            return userService;
        }
}
